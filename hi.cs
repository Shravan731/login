using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ProductApplication.Models
{
    public class ImgClass
    {
        [Key]
        public int ImgId { get; set; }

        public string Imgpath { get; set; }

        public string Imgname { get; set; }

    }
}
